import React from "react";
import PropTypes from "prop-types";
import "./styles.css";

interface IState {}

interface IProps {
  participants: number;
  messagesNumber: number;
  chatName: string;
  lastMessage: string;
}

class ChatHeader extends React.Component<IProps, IState> {
  static propTypes = {
    participants: PropTypes.number,
    messagesNumber: PropTypes.number,
    chatName: PropTypes.string,
    lastMessage: PropTypes.string,
  };

  render() {
    return (
      <div className="chatHeader header">
        <span className="headerStyle">
          <b>Name:</b> <span className="header-title">{this.props.chatName}</span>
        </span>
        <span className="headerStyle">
          <b>Participants:</b> <span className="header-users-count">{this.props.participants}</span>
        </span>
        <span className="headerStyle">
          <b>Messages Number:</b> <span className="header-messages-count">{this.props.messagesNumber}</span>
        </span>
        <span className="headerStyle" id="lastMessage">
          <b>Last message at </b>
          <span className="header-last-message-date">{this.props.lastMessage}</span>
        </span>
      </div>
    );
  }
}

export default ChatHeader;
