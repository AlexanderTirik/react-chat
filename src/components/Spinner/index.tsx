import React from "react";
import "./styles.css";
class Spinner extends React.Component {
  render() {
    return <div className="loader preloader"></div>;
  }
}
export default Spinner;
