import React from "react";
import "./App.css";
import Chat from "./Chat";
import Header from "./Header"
import Footer from "./Footer"

function App() {
  return (
    <div className="App">
      <Header/>
      <Chat url="https://api.npoint.io/a139a0497ad54efd301f"/>
      <Footer/>
    </div>
  );
}

export default App;
